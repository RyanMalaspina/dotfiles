#!/usr/bin/env bash

function run {
	if ! pgrep $1;
	then
		$@&
	fi
}

# Networking
run nm-applet
run blueman-applet

# Easy on the eyes
run redshift-gtk
run xbacklight -set 15
run xscreensaver -no-splash

# Very generic categorization of 'Files'
run udiskie --no-automount --tray
run nextcloud

# Mirror
forever start ~/git-repos/pushbullet-node-mirror/index.js

# Kill the touchpad
#run ./disable-touchpad.py

